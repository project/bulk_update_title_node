CONTENTS OF THIS FILE

* Introduction
* Requirements
* Installation
* Configuration
* Troubleshooting
* FAQ
* Maintainers


INTRODUCTION
------------

Bulk Update Title Node module allows to update titles of nodes depends on the content type selected.


REQUIREMENTS
------------

No special requirements.s

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
  https://www.drupal.org/node/1897420
  for further information.
* After installation, Go to /admin/content/bulk-update.
* Select the content type, the old title and the new title and click on replace.
* All titles which have the content types selected will be replaced.

CONFIGURATION
-------------

*
*
*

TROUBLESHOOTING
---------------

*
*
*

FAQ
---------------

*
*
*

MAINTAINERS
-----------

* Mohamed Anis Taktak (matio89) - https://www.drupal.org/u/matio89
