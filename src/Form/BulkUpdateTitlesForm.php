<?php

namespace Drupal\bulk_update_title_node\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BulkUpdateTitlesForm.
 *
 * @package Drupal\bulk_update_title_node\Form
 */
class BulkUpdateTitlesForm extends FormBase {


  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new TitleReplaceWordForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')

    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bulk_update_title_node';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Content Type'),
      '#options' => $this->getTypes(),
      '#required' => TRUE,
    ];

    $form['bulk_update_title_node_from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Partial or full old title'),
      '#required' => TRUE,
    ];
    $form['bulk_update_title_node_to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Partial or full new title'),
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['next'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Replace'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $content_type = $form_state->getValue('content_type');
    $existing_word = $form_state->getValue('bulk_update_title_node_from');
    $storage = $this->entityTypeManager->getStorage('node');
    $nodes = $storage->getQuery()
      ->accessCheck(TRUE)
      ->condition('type', $content_type)
      ->condition('title', '%' . Database::getConnection()
        ->escapeLike($existing_word) . '%', 'LIKE')
      ->execute();
    $number_nodes = count($nodes);
    if ($number_nodes == 0) {
      $form_state->setError($form, $this->t('Sorry, we didn\'t find any title that match with your search, please try again!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $content_type = $form_state->getValue('content_type');
    // Old word.
    $from = $form_state->getValue('bulk_update_title_node_from');
    // New word.
    $to = $form_state->getValue('bulk_update_title_node_to');
    $storage = $this->entityTypeManager->getStorage('node');
    // Get list of nodes which have the content type selected and match with title.
    $nodes = $storage->getQuery()
      ->accessCheck(TRUE)
      ->condition('type', $content_type)
      ->condition('title', '%' . Database::getConnection()
        ->escapeLike($from) . '%', 'LIKE')
      ->execute();
    // Initialization batch.
    $batch = [
      'title' => $this->t('Processing updates on titles...'),
      'operations' => [],
      'finished' => [get_class($this), 'finishBatch'],
    ];
    if ($nodes) {
      foreach ($nodes as $nid) {
        $batch['operations'][] = [
          [get_class($this), 'processBatch'],
          [$nid, $from, $to],
        ];
      }
      batch_set($batch);
    }

  }

  /**
   * Get content types list.
   *
   * @return array
   *   Content types list.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTypes() {
    // Get all content types.
    $content_types = $this->entityTypeManager->getStorage('node_type')
      ->loadMultiple();
    // Initialization list titles.
    $content_types_list = [];
    foreach ($content_types as $content_type) {
      $content_types_list[$content_type->id()] = $content_type->label();
    }
    return $content_types_list;
  }

  /**
   * Processes the batch item.
   *
   * @param int $nid
   *   The node id.
   * @param string $from
   *   The old partial or full title.
   * @param string $to
   *   The new partial or full title.
   * @param array $context
   *   The batch context.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function processBatch($nid, $from, $to, &$context) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = Node::load($nid);

    $title = $node->getTitle();
    $message = t('Updating title: %title', ['%title' => $title]);
    $results = [];
    // Replace the old title.
    $new_title = str_ireplace($from, $to, $title);
    $node->setTitle($new_title);
    $node->save();

    $results[] = $new_title;
    $context['message'] = $message;
    $context['results'][] = $results;
  }

  /**
   * Finish batch.
   *
   * @param bool $success
   *   Indicates whether the batch process was successful.
   * @param array $results
   *   Results information passed from the processing callback.
   */
  public static function finishBatch($success, $results) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One content updated.', '@count contents updated.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addStatus($message);
  }

}
